<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Selamat Datang di Sistem Pembelajaran Sejarah</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/business-casual.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    

</head>

  <header>
      <nav class="navbar nav navbar-default" role="navigation">
        <div class="container">
             <!-- Brand -->
          <div class="navbar-header">
          <ul class="nav navbar-nav navbar-right">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">Sistem Pembelajaran Sejarah</a>
            </ul>
          </div>

          <!-- Menu -->
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="">Beranda</a></li>
            <li><a href="#">Profil<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Profil</a></li>
                    <li><a href="#">Keluar</a></li>
                </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

<body>
        <div class="container">
            <div class="box" id="body_post" class="center">
                <div class="center">
                    <h2>Laboratorium</h2>
                </div>
                <ul class="portfolio-items col-3">
                    <li class="portfolio-item apps">
                     
                        <div class="item-inner" >
                            <div class="portfolio-image">
                                <img  src="images/lab 1.png" alt="" style="width:250px; height=250px ">
                                <div class="overlay">
                                    <a class="preview btn btn-danger" title="Lab. Fisiologi & Sport Medicine" href="images/Observasi Lab. FPOK/Lab. Fisiologi & Sport Medicine/ruang D/P_20151217_133231.jpg"><i class="icon-eye-open"></i></a>             
                                </div>
                            </div>
                        </div>   
                     
                    </li><!--/.portfolio-item-->
                    <li class="portfolio-item joomla bootstrap">
                      
                        <div class="item-inner">
                            <div class="portfolio-image">
                                <img src="images/lab 2.png" style="width:250px; height=250px ">
                                <div class="overlay">
                                    <a class="preview btn btn-danger" title="Lab. Tes & Pengukuran" href="images/Observasi Lab. FPOK/Lab. Tes & Pengukuran/image2993.png"><i class="icon-eye-open"></i></a>  
                                </div>
                            </div>
                        </div>
                    
                </ul>   
                
            </div><!--/.box-->
        </div><!--/.container-->
    <!-- /.container -->


    <script src="js/jquery.js"></script>

    <script src="js/bootstrap.min.js"></script>


</body>


<!--<footer>
Sistem Pembelajaran Sejarah <br/>
RPL Kelompok 9 | Pendidikan Ilmu Komputer A 2014
</footer>-->


</html>
