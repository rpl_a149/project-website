<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistem Pembelajaran Sejarah</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/business-casual.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    

</head>

  <header>
      <nav class="navbar nav navbar-default" role="navigation">
        <div class="container">
             <!-- Brand -->
          <div class="navbar-header">
          <ul class="nav navbar-nav navbar-right">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">Sistem Pembelajaran Sejarah</a>
            </ul>
          </div>

          <!-- Menu -->
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="">Beranda</a></li>
            <li><a href="#" class="dropdown-toggle" id="dLabel" role="button" data-toggle="dropdown">Nama Siswa<b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <li><a href="#">Profil</a></li>
                    <li><a href="#">Keluar</a></li>
                </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

<body>
        
        <div class="container">
        <div class="box_jd">
            <h3>Tugas</h3>
        </div>

            <div class="box" id="body_post" class="center">

            <div class="row kuis">
              <div class="container jd_kuis col-md-11" >Tugas Sejarah 28 Oktober</div>
              <a href="#"><div class="col-md-1 btn btn-primary">Ikuti Kuis</div></a>
            </div>        

            <div class="row kuis">
              <div class="container jd_kuis col-md-11" >Tugas Bandung Lautan Api</div>
              <a href="#"><div class="col-md-1 btn btn-primary">Ikuti Kuis</div></a>
            </div>  

            </div><!--/.box-->
        </div><!--/.container-->
    <!-- /.container -->


    <script src="js/jquery.js"></script>

    <script src="js/bootstrap.min.js"></script>


</body>


<!--<footer>
Sistem Pembelajaran Sejarah <br/>
RPL Kelompok 9 | Pendidikan Ilmu Komputer A 2014
</footer>-->


</html>
