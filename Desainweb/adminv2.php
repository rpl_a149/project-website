<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistem Pembelajaran Sejarah</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/business-casual.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    

</head>

  <header>
      <nav class="navbar nav navbar-default" role="navigation">
        <div class="container">
             <!-- Brand -->
          <div class="navbar-header">
          <ul class="nav navbar-nav navbar-right">
                <a href="#" class="navbar-brand">Sistem Pembelajaran Sejarah</a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>

                    
                </button>
            </ul>
          </div>

          <!-- Menu -->
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="">Beranda</a></li>
            <li><a href="#" class="dropdown-toggle" id="dLabel" role="button" data-toggle="dropdown">Nama Guru<b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <li><a href="#">Profil</a></li>
                    <li><a href="#">Keluar</a></li>
                </ul>
            </li>
          </ul>
        </div>
      </nav>


    </header>

<body>
        <div class="container bg_panel">

<!-- Panel atas -->
            <div class="panel_admin col-md-12">
                        <ul class="nav nav-tabs">

                          <li class="dropdown">
                            <a class="dropdown-toggle"
                               data-toggle="dropdown"
                               href="#">
                                Tugas
                                <b class="caret"></b>
                              </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Buat Soal</a></li>
                                <li><a href="#">Edit Soal</a></li>
                            </ul>
                          </li>

                            <li class="dropdown">
                            <a class="dropdown-toggle"
                               data-toggle="dropdown"
                               href="#">
                                Siswa
                                <b class="caret"></b>
                              </a>
                            <ul class="dropdown-menu">
                              <!-- links -->
                                <li><a href="#">Registrasi Siswa</a></li>
                                <li><a href="#">Edit Siswa</a></li>
                                <li><a href="#">Lihat Siswa</a></li>
                            </ul>
                          </li>

                            <li class="dropdown">
                            <a class="dropdown-toggle"
                               data-toggle="dropdown"
                               href="#">
                                Nilai
                                <b class="caret"></b>
                              </a>
                            <ul class="dropdown-menu">
                              <li><a href="#">Lihat Nilai</a></li>
                            </ul>
                          </li>

                        </ul>


            </div><!--/.box-->


<!-- Panel bawah -->
                <div class="panel_konten col-md-12">
                    ....
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

                </div>
        </div><!--/.container-->






    <script src="js/jquery.js"></script>

    <script src="js/bootstrap.min.js"></script>


</body>


<!--<footer>
Sistem Pembelajaran Sejarah <br/>
RPL Kelompok 9 | Pendidikan Ilmu Komputer A 2014
</footer>-->


</html>
