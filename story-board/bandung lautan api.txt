SUATU hari di Bulan Maret 1946, dalam waktu tujuh jam, sekitar 200.000 penduduk mengukir sejarah dengan membakar rumah dan harta benda mereka, meninggalkan kota Bandung menuju pegunungan di selatan. Beberapa tahun kemudian, lagu "Halo-Halo Bandung" ditulis untuk melambangkan emosi mereka, seiring janji akan kembali ke kota tercinta, yang telah menjadi lautan api.

Insiden Perobekan Bendera

Setelah Proklamasi Kemerdekaan 17 Agustus 1945, Indonesia belum sepenuhnya merdeka. Kemerdekaan harus dicapai sedikit demi sedikit melalui perjuangan rakyat yang rela mengorbankan segalanya. Setelah Jepang kalah, tentara Inggris datang untuk melucuti tentara Jepang. Mereka berkomplot dengan Belanda (tentara NICA) dan memperalat Jepang untuk menjajah kembali Indonesia.

Berita pembacaan teks Proklamasi Kemerdekaan dari Jakarta diterima di Bandung melalui Kantor Berita DOMEI pada hari Jumat pagi, 17 Agustus 1945. Esoknya, 18 Agustus 1945, cetakan teks tersebut telah tersebar. Dicetak dengan tinta merah oleh Percetakan Siliwangi. Di Gedung DENIS, Jalan Braga (sekarang Gedung Bank Jabar), terjadi insiden perobekan warna biru bendera Belanda, sehingga warnanya tinggal merah dan putih menjadi bendera Indonesia. Perobekan dengan bayonet tersebut dilakukan oleh seorang pemuda Indonesia bernama Mohammad Endang Karmas, dibantu oleh Moeljono.

Tanggal 27 Agustus 1945, dibentuk Badan Keamanan Rakyat (BKR), disusul oleh terbentuknya Laskar Wanita Indonesia (LASWI) pada tanggal 12 Oktober 1945. Jumlah anggotanya 300 orang, terdiri dari bagian pasukan tempur, Palang Merah, penyelidikan dan perbekalan.

Peristiwa yang memperburuk keadaan terjadi pada tanggal 25 November 1945. Selain menghadapi serangan musuh, rakyat menghadapi banjir besar meluapnya Sungai Cikapundung. Ratusan korban terbawa hanyut dan ribuan penduduk kehilangan tempat tinggal. Keadaan ini dimanfaatkan musuh untuk menyerang rakyat yang tengah menghadapi musibah.


Berbagai tekanan dan serangan terus dilakukan oleh pihak Inggris dan Belanda. Tanggal 5 Desember 1945, beberapa pesawat terbang Inggris membom daerah Lengkong Besar. Pada tanggal 21 Desember 1945, pihak Inggris menjatuhkan bom dan rentetan tembakan membabi buta di Cicadas. Korban makin banyak berjatuhan.

Bandoeng Laoetan Api

Ultimatum agar Tentara Republik Indonesia (TRI) meninggalkan kota dan rakyat, melahirkan politik "bumihangus". Rakyat tidak rela Kota Bandung dimanfaatkan oleh musuh. Mereka mengungsi ke arah selatan bersama para pejuang. Keputusan untuk membumihanguskan Bandung diambil melalui musyawarah Majelis Persatuan Perjuangan Priangan (MP3) di hadapan semua kekuatan perjuangan, pada tanggal 24 Maret 1946.

Kolonel Abdul Haris Nasution selaku Komandan Divisi III, mengumumkan hasil musyawarah tersebut dan memerintahkan rakyat untuk meninggalkan Kota Bandung. Hari itu juga, rombongan besar penduduk Bandung mengalir panjang meninggalkan kota.

Bandung sengaja dibakar oleh TRI dan rakyat dengan maksud agar Sekutu tidak dapat menggunakannya lagi. Di sana-sini asap hitam mengepul membubung tinggi di udara. Semua listrik mati. Inggris mulai menyerang sehingga pertempuran sengit terjadi. Pertempuran yang paling seru terjadi di Desa Dayeuhkolot, sebelah selatan Bandung, di mana terdapat pabrik mesiu yang besar milik Sekutu. TRI bermaksud menghancurkan gudang mesiu tersebut. Untuk itu diutuslah pemuda Muhammad Toha dan Ramdan. Kedua pemuda itu berhasil meledakkan gudang tersebut dengan granat tangan. Gudang besar itu meledak dan terbakar, tetapi kedua pemuda itu pun ikut terbakar di dalamnya. Staf pemerintahan kota Bandung pada mulanya akan tetap tinggal di dalam kota, tetapi demi keselamatan maka pada jam 21.00 itu juga ikut keluar kota. Sejak saat itu, kurang lebih pukul 24.00 Bandung Selatan telah kosong dari penduduk dan TRI. Tetapi api masih membubung membakar kota. Dan Bandung pun berubah menjadi lautan api.

Pembumihangusan Bandung tersebut merupakan tindakan yang tepat, karena kekuatan TRI dan rakyat tidak akan sanggup melawan pihak musuh yang berkekuatan besar. Selanjutnya TRI bersama rakyat melakukan perlawanan secara gerilya dari luar Bandung. Peristiwa ini melahirkan lagu "Halo-Halo Bandung" yang bersemangat membakar daya juang rakyat Indonesia.

Bandung Lautan Api kemudian menjadi istilah yang terkenal setelah peristiwa pembakaran itu. Banyak yang bertanya-tanya darimana istilah ini berawal. Almarhum Jenderal Besar A.H Nasution teringat saat melakukan pertemuan di Regentsweg (sekarang Jalan Dewi Sartika), setelah kembali dari pertemuannya dengan Sutan Sjahrir di Jakarta, untuk memutuskan tindakan apa yang akan dilakukan terhadap Kota Bandung setelah menerima ultimatum Inggris.

    Jadi saya kembali dari Jakarta, setelah bicara dengan Sjahrir itu. Memang dalam pembicaraan itu di Regentsweg, di pertemuan itu, berbicaralah semua orang. Nah, disitu timbul pendapat dari Rukana, Komandan Polisi Militer di Bandung. Dia berpendapat, �Mari kita bikin Bandung Selatan menjadi lautan api.� Yang dia sebut lautan api, tetapi sebenarnya lautan air�
    A.H Nasution, 1 Mei 1997

Istilah Bandung Lautan Api muncul pula di harian Suara Merdeka tanggal 26 Maret 1946. Seorang wartawan muda saat itu, yaitu Atje Bastaman, menyaksikan pemandangan pembakaran Bandung dari bukit Gunung Leutik di sekitar Pameungpeuk, Garut. Dari puncak itu Atje Bastaman melihat Bandung yang memerah dari Cicadas sampai dengan Cimindi.

Setelah tiba di Tasikmalaya, Atje Bastaman dengan bersemangat segera menulis berita dan memberi judul Bandoeng Djadi Laoetan Api. Namun karena kurangnya ruang untuk tulisan judulnya, maka judul berita diperpendek menjadi Bandoeng Laoetan Api.